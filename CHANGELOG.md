## [0.1.1](https://gitlab.com/mfgames-writing/mfgames-writing-html-js/compare/v0.1.0...v0.1.1) (2018-08-11)


### Bug Fixes

* updating package mangagement ([db7daf7](https://gitlab.com/mfgames-writing/mfgames-writing-html-js/commit/db7daf7))
